.. image:: https://img.shields.io/badge/licence-LGPL--3-blue.svg
   :target: https://www.gnu.org/licenses/agpl
   :alt: License: LGPL-3

Season Global
=============
This module add a field on res.company to store header image
used in odoo documents. ie. reports


