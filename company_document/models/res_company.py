# -*- coding: utf-8 -*-

from odoo import models, fields


class ResCompany(models.Model):
    _inherit = "res.company"

    header_image = fields.Binary(string="Report Header")
    footer_image = fields.Binary("Report Footer")
    stamp_image = fields.Binary('Stamp')
