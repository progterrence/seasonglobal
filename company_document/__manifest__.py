# -*- coding: utf-8 -*-
{
    'name': 'Company Document',
    'version': '12.0.0.0',
    'category': 'Company',
    'sequence': 1,
    'summary': 'Company Document',
    'author': 'Terence Nzaywa',
    'website': '',
    'depends': [
        'base',
        'web'
    ],
    'data': [
        'views/res_company.xml'
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
}