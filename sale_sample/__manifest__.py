{
    'name': 'Sales Samples',
    'version': '1.0',
    'category': 'Sale',
    'sequence': 1,
    'summary': 'Sale Samples',
    'author': '',
    'depends': ['sale_management'],
    'data': [
        'security/ir.model.access.csv',
        'views/sale_views.xml',
        'views/sale_sample.xml'
    ],
    'demo': [],
    'installable': True,
    'auto_install': False,
    'application': False,
}
